import React from 'react';

const Poster = (props) => {
    return (
        <div className="poster">
            <img src={props.link} alt="#" width="220px" height="300px" className="img"/>
            <h4 className="title"><b>{props.name}</b></h4>
            <p className="year">{props.year}</p>
        </div>
    );
};
export default Poster;